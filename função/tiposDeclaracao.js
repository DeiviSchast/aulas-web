// function declaration

function soma(x,y){
    return x + y;
}

// function expression

const sub = function(x,y) {
    return x - y;
}

// named function expression

const mult = function mult(x,y){
    return x * y;
}
console.log(soma(2,3));
console.log(sub(2,3));
console.log(mult(2,3));