const contadorA = require('./instanciaUnica');
const contadorB = require('./instanciaNova')();

const contadorC = require('./instanciaNova')();
const contadorD = require('./instanciaNova')();

contadorA.inc();
contadorA.inc();
console.log(contadorA.valor, contadorB.valor);

contadorC.inc();
contadorD.inc();
console.log(contadorC.valor, contadorD.valor);