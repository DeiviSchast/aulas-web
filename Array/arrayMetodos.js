const pilotos = ['Vettel', 'Alonso', 'Raikkonen', 'Massa'];

pilotos.pop(); // remove ultimo elemtno do array
console.log(pilotos);

pilotos.push('Verstappen');
console.log(pilotos);

pilotos.shift(); // remove o primeiro elemento da lista
console.log(pilotos);

pilotos.unshift('Hamilton');
console.log(pilotos);

// splice pode adicionar e remover elementos

pilotos.splice(2, 0, 'Bottas', 'Massa');
console.log(pilotos);
pilotos.splice(3, 1); // massa sai de novo
console.log(pilotos);

const algunsPilotos1 = pilotos.slice(2); // novo array gerado a partir do indice indicado
console.log(algunsPilotos1);

const algunsPilotos2 = pilotos.slice(1, 4);
console.log(algunsPilotos2);
