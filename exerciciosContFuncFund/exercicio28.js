function quantidadeParesImpares(vetorNumeros){
    let qtdpares = 0;
    let qtdimpares = 0;
    for (let i = 0; i < vetorNumeros.length; i++){
        if(vetorNumeros[i] % 2 == 0){
            qtdpares++;
        } else{
            qtdimpares++;
        }
    }
    console.log(`Quantidade de pares: ${qtdpares}. Quantidade de impares: ${qtdimpares}`);
}
vetor = [1,2,3,4,5,6,7,8,9,10,11,12,13];
quantidadeParesImpares(vetor);