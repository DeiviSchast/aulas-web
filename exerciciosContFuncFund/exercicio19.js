function cardapio(qtd, codigo){
    switch(codigo){
        case 100:
            console.log('Valor da compra: R$ ' + (qtd * 3.00).toFixed(2));
            break;
        case 200:
            console.log('Valor da compra: R$ ' + (qtd * 4.00).toFixed(2));
            break;
        case 300:
            console.log('Valor da compra: R$ ' + (qtd * 5.50).toFixed(2));
            break;
        case 400:
            console.log('Valor da compra: R$ ' + (qtd * 7.50).toFixed(2));
            break;
        case 500:
            console.log('Valor da compra: R$ ' + (qtd * 3.50).toFixed(2));
            break;
        case 600:
            console.log('Valor da compra: R$ ' + (qtd * 2.80).toFixed(2));
            break;
        default:
            console.log('Entrada invalida');
            break;
    }

}

cardapio(2, 100);
cardapio(2, 400);