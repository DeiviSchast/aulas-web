function calculadora(valor1, operacao, valor2){
    switch(operacao){
        case '+':
            return valor1 + valor2;
            break;
        case '-':
            return valor1 - valor2;
            break;
        case '*':
            return valor1 * valor2;
            break;
        case '/':
            return valor1/valor2;
            break;
        default:
            return 'Operacao nao cadastrada';
            break;
    }
}

console.log(calculadora(2, '+', 30));
console.log(calculadora(2, '/', 4));