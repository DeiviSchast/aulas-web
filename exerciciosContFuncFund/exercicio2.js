function triangulo(lado1, lado2, lado3){
    if (lado1 == lado2 && lado2 == lado3){
        console.log('Triangulo Equilatero');
    }else if (lado1 == lado2 || lado1 == lado3 || lado2 == lado3){
        console.log('Triangulo Isosceles');
    }else{
        console.log('Triangulo Escaleno');
    }
}
triangulo(3,3,3);
triangulo(3,2,2);
triangulo(2,1,3);
triangulo(2,3,2);