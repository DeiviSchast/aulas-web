function planoTrabalho(salario, plano){
    switch(plano){
        case 'A':
            return salario * 1.10;
            break;
        case 'B':
            return salario * 1.15;
            break;
        case 'C':
            return salario * 1.20;
            break;
        default:
            return 'Plano invalido';
    }
}

console.log(planoTrabalho(1000, 'A'));
console.log(planoTrabalho(1000, 'B'));
console.log(planoTrabalho(1000, 'C'));
console.log(planoTrabalho(1000, 'D'));
