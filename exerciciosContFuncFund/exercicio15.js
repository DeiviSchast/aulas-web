function comprarCarro(nome){
    switch(nome){
        case 'sedan':
            console.log('Compra efetuada com sucesso!');
            break;
        case 'motocicletas':
        case 'caminhonetes':
            console.log('Tem certeza que nao prefere esse modelo?');
            break;
        default:
            console.log('Nao trabalhamos com este tipo de automovel');
            break;
    }
}

comprarCarro('sedan');
comprarCarro('motocicletas');
comprarCarro('hatch');