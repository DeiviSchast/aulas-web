function exponencial(base, expoente){
    if(expoente >= 0){
        return base**expoente;
    } else if(expoente == 0){
        return 1;
    } else{
        return 1/(base**(-expoente));
    }
}
console.log(exponencial(3,3));
console.log(exponencial(2,-3));
console.log(exponencial(2,0));
console.log(exponencial(2,-5));
