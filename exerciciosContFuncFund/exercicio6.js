// Juros simples => J = C.i.t (Juros = Capital x taxa x tempo)
// Juros composto => S = P.(1+i)**n (Montante = Principal(taxa + 1)**tempo)


function jurosSimples(capitalInicial, taxa, tempo){
    return capitalInicial + (capitalInicial * taxa * tempo);
}

function jurosComposto(capitalInicial, taxa, tempo){
    return capitalInicial + (capitalInicial * ((taxa + 1)**tempo));
}

console.log(jurosSimples(100, 0.1, 2));