// Juros composto => S = P.(1+i)**n (Montante = Principal(taxa + 1)**tempo)

function calcularValor(mes, valor){
    if(mes > 0 && mes < 13){
        atraso = mes - 1;
        return (valor * ((1 + (5/100))**atraso)).toFixed(2);
    } else{
        return 'Mes invalido';
    }
}
console.log(calcularValor(4, 100));